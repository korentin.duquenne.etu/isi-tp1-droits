import os

def main():
    gid = os.getegid()
    uid = os.geteuid()
    print(" euid : ",end="")
    print(uid,end="  ")
    print(" egid : ",end="")
    print(gid)
    try:
        file = open("mydir/data.txt","r")
        str = file.readline()
        while str !="":
            print(str)
            str = file.readline()
        close()
    finally:
        return 0
        
    
if __name__ == "__main__":
    main()
