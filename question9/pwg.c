#include "checkpass.h"


// Fonction qui change le password d'un user déja dans le fichier 
void changePswd(int line){
  FILE *file;
  FILE *tempFile; 
  char new_psw[256];
  char *new_psw_crypt;
  char salt[] = "$1$........";
  printf("Votre Nouveau mot de passe : \n");
  scanf("%s",new_psw); // On demande le nouveau mot de passe à remplacer 
  new_psw_crypt=crypt(new_psw,salt);
  if((file = fopen("/home/admin/pssw","r"))== NULL){ // On ouvre le fichier 
    printf("ERROR ! OPENING FILE");
    exit(1);
  }
  if((tempFile = fopen("/home/admin/changepass.txt","w"))== NULL){ // On ouvre le fichier temporaire ou nous allons appliquer les changments
    printf("ERROR ! OPENING TEMPORY FILE");
    exit(1);
  }
  char *l = NULL;
  char *mdp = NULL;
  size_t len = 0;
  ssize_t read;
  char *token = NULL;
  int j = 0;
  while((read = getline(&l,&len,file)!=-1)){
    if(j == line){ // Si la ligne et celle qui doit être changer on ecris la nouvelle ligne avec nouveau mdp dans le fichier tmp
      token = strtok(l,":");
      fputs(token,tempFile);
      fputs(":",tempFile);
      fputs(new_psw_crypt,tempFile);
      fputs("\n",tempFile);
    }
    else{ // sinon on recris la ligne dans le fichier tmp
      fputs(l,tempFile);
    }
    j = j +1;
  }
  fclose(file);
  fclose(tempFile);
  remove("/home/admin/pssw"); // on supprime l'ancien fichier 
  rename("/home/admin/changepass.txt", "home/admin/pssw"); // on renome le fichier tmp en nouveau
}

// Fonction qui renvoie la ligne ou est stocké le mdp 
int checkLine(int uid){
  int id = uid;
  char *line = NULL;
  char *mdp = NULL;
  size_t len = 0;
  ssize_t read;
  char *token = NULL;
  int tmp;
  FILE *file; 
  // On ouvre le fichier de mdp 
  if((file = fopen("/home/admin/pssw","r"))== NULL){
    printf("ERROR ! OPENING FILE");
    exit(1);
  }
  tmp = 0;
  // on lit chaque ligne du fichier de psw
  while((read = getline(&line,&len,file)!=-1)){
      token = strtok(line,":");
      int userid;
      userid = atoi(token);
      // si l'uid du user = le userd id de la ligne 
      if(uid == userid){
	fclose(file);
	return tmp; // on renvoie la ligne 
      }
      tmp = tmp +1;
  }
  fclose(file);
  return -1;
}

void addPswd(int uid){
  FILE *file;
  char pass[256];
  char salt[] = "$1$........";
  char *password;
  scanf("%s",pass);
  password = crypt(pass,salt);
  if((file = fopen("/home/admin/pssw","a+"))== NULL){
    printf("ERROR ! OPENING FILE");
    exit(1);
  }
  fprintf(file,"%d:%s\n",uid,password);
  fclose(file);
}

int main(int argc, char * argv[]){
  
  char *pass;
  int line;
  int uid = getuid();
  int res = checkpass(uid);
  if(res==0){ // si l'utilisateur à déja 
    line = checkLine(uid);
    printf("%d -> line \n",line);
    changePswd(line);
    return 0; 
  }
  else if(res == -1){
    addPswd(uid);
    return 0;
  }
  else{
    printf("MAUVAIS MOT DE PASSE");
    return 1
  }
}





