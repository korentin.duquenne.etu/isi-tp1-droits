#include "checkpass.h"


int checkpass(int id){
  int uid = id;
  FILE *file;
  // On ouvre le fichier de mdp 
  if((file = fopen("/home/admin/pssw","r"))== NULL){
    printf("ERROR ! OPENING FILE");
    exit(1);
  }
  char *line = NULL;
  char *mdp = NULL;
  size_t len = 0;
  ssize_t read;
  char *token = NULL;
  int res;
  char salt[] = "$1$........";
  // on lit chaque ligne du fichier de psw
  while((read = getline(&line,&len,file)!=-1)){
      token = strtok(line,":");
      int userid;
      userid = atoi(token);
      // si l'uid du user = le userd id de la ligne 
      if(uid == userid){
	// On Compare le mdp à l'input demander au user
	mdp = strtok(NULL,":");
	mdp = strtok(mdp,"\n");
	char mdp2[64];
	char *mdp2_crypt;
	printf("Votre Mot de passe :\n");
	if(scanf("%s",mdp2)==-1){
	  perror("error");
	}
	mdp2_crypt = crypt(mdp2,salt);
	fclose(file);
	// On retourne la comparaison entre le mdp et l'input du user
	res = strcmp(mdp,mdp2_crypt);
	if(res!=0){
	  printf("Mauvais mot de passe \n");
	  return 1;
	}
	return 0;
      }
  }
  fclose(file);
  return -1;
}
