# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

* DUQUENNE, Korentin, email: korentin.duquenne.etu@univ-lille.fr
* BONVOISIN, Alexandre, email: alexandre.bonvoisin.etu@univ-lille.fr

## Question 1

Le processus lancé par toto ne peut pas écrire dans le fichier, même si le fichier appartient à toto, car il n'a pas le droit d'écriture sur ce fichier.  
De plus nous remarquons que toto est membre du groupe 'ubuntu' et que ce groupe à le droit d'écrire dans le fichier, mais que ces droits d'utilisateur l'importe sur les droits du groupe.  

### Trace 

Vous pouvez consulter la trace d'exécution de cette question dans le fichier `question1/question1.sh`.  

## Question 2

* Le caractère "x" pour un répertoire, signifie que l'on peut exécuter des processus ciblant ce répertoire.
* Nous avons un message "Permission denied" car nous n'avons pas le droit d'exécution pour le groupe "ubuntu" pour ce répertoire.
* Nous avons un message "Permission denied" pour chacun des répertoires et fichiers présent dans ce dossier, car pour obtenir les informations sur ces fichiers il faut avoir le droit d'exécution sur ce répertoire.

### Trace 

Vous pouvez consulter la trace d'exécution de cette question dans le fichier `question2/question2.sh`.  

## Question 3

Le processus n'arrive pas à ouvrir le fichier texte et les valeurs des ids sont :
	ruid : 1001, euid : 1001, rgid : 1001, egid : 1001

Après activation du flag set-user-id le processus arrive à ouvrir le fichier texte et les valeurs des ids sont :
	ruid : 1001, euid : 1000, rgid : 1001, egid : 1001

## Question 4

Le processus py n'arrive pas à ouvrir le fichier texte et les valeurs des ids sont:
	euid : 1001   egid : 1001

Après activiation du flag set-user-id le processus arrive à ouvrir le fichier texte et les valeurs des ids sont : 
	euid : 1000   egid : 1001

## Question 5

La commande "chfn" vous permet de modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd".

-rwsr-xr-x 1 root root

Il n'y a que le root qui puisse changer tous les utilisateur et un utilisateur ne peux changer juste ses propres informations

toto:x:1001:1001:toto,toto,toto,toto,toto:/home/toto:/bin/bash  -> avant changement

toto:x:1001:1001:toto,1,1,1,toto:/home/toto:/bin/bash -> après changement

## Question 6

Les mots de passe dans les système Unix/Linux sont stockés dans le fichier ‘/etc/shadow’. Dans les versions récentes des systèmes Unix, les mots de passe sont stockés dans le fichier ‘/etc/shadow’ et ne peuvent être lu que par le root.

C'est fait de sorte qu'il n'y a que le superutilisateur qui puisse voire les mot de passes et eviter qu'un utilisateur lambda puisse voir accés à d'autre compte utilisateur

## Question 7

les scripts bash sont dans le repertoire *question7*.

Script.sh Permet de voir les commande utilisé pour la question 7
test_admin.sh permet d'utiliser des commmande pour tester admin ( Il faut avoir les dir_a,dir_b,dir_c dans /home et avoir toto.txt dans chaque repertoire)
test_lambda_a.sh permet d'utiliser des commmande pour tester lambda_a ( Il faut avoir les dir_a,dir_b,dir_c dans /home et avoir toto.txt dans dir_b et dir_c)
test_lambda_b.sh permet d'utiliser des commmande pour tester lambda_b ( Il faut avoir les dir_a,dir_b,dir_c dans /home et avoir toto.txt dans dir_a et dir_c)


## Question 8

Le programme et les scripts dans le repertoire *question8*.

script.sh permet de voir de lancer un script pour supprimer un fichier en fonction de admin

Si vous avez des questions sur le code n'hesitez pas à envoyer un mail pour des informations

## Question 9

Le programme et les scripts dans le repertoire *question9*.

script_pwg.sh permet de lancer un script pour creer un mdp pour l'admin et lui changer de mot de passe 

script_rwg2.sh permet de lancer un script pour supprimer un fichier en fonction de l'utilisateur admin

Si vous avez des questions sur le code n'hesitez pas à envoyer un mail pour des informations

## Question 10

N'a pas été encore traité 







