#include "checkpass.h"


int checkpass(int id){
  int uid = id;
  FILE *file;
  // On ouvre le fichier de mdp 
  if((file = fopen("/home/admin/psswd","r"))== NULL){
    printf("ERROR ! OPENING FILE");
    exit(1);
  }
  char *line = NULL;
  char *mdp = NULL;
  size_t len = 0;
  ssize_t read;
  char *token = NULL;
  // on lit chaque ligne du fichier de psw
  while((read = getline(&line,&len,file)!=-1)){
      token = strtok(line,":");
      int userid;
      userid = atoi(token);
      // si l'uid du user = le userd id de la ligne 
      if(uid == userid){
	// On Compare le mdp à l'input demander au user
	mdp = strtok(NULL,":");
	mdp = strtok(mdp,"\n");
	char mdp2[64];
	scanf("%s",mdp2);
	fclose(file);
	// On retourne la comparaison entre le mdp et l'input du user
	return strcmp(mdp,mdp2);
      }
  }
  fclose(file);
  return 1;
}
