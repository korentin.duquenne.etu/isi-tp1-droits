#include "checkpass.h"

int main(int argc, char *argv[]){

  struct stat sb;
  stat(argv[1],&sb);

  // On recupére le group du fichier 
  struct group *gr = getgrgid(sb.st_gid);
  //printf("%s GROUPE OF FILE \n",gr->gr_name);

  // on recupere la structure lié au user qui essaie de rm le fichier
  __uid_t uid = getuid();
  struct passwd* pw = getpwuid(uid);
  if(pw == NULL){
    perror("getpwuid error:");
  }

  int ngroups=0;
  getgrouplist(pw->pw_name,pw->pw_gid,NULL,&ngroups);
  __gid_t groups[ngroups];
  getgrouplist(pw->pw_name,pw->pw_gid,groups,&ngroups);

  int g = 1;
  struct group *gr2;

  // On regarde si la personne appartient bien au groupe du fichier 
  for (int i = 0; i<ngroups;i++){
    gr2 = getgrgid(groups[i]);
    //printf("%s GROUP OF USER \n",gr2->gr_name);
    if(gr2->gr_name == gr->gr_name){
      g = 0;
    }
  }
  // Si non on retourne qu'il ne peut pas remove le fichier 
  if(g==1){
    printf("You don't have the right to remove the file\n");
    exit(1);
  }
  // Si oui on lui demande un psw
  int i = checkpass(uid);
  // si le psw est bon 
  if(i==0){
    // On essaie de de supprimer le fichier 
    int ret = remove(argv[1]);
    if(ret == 0){
      printf("File deleted successfully\n");
    }
    else{
      printf("Error : unable to delete the file\n");
    }
    return 0;
  }
  // Si le psw est mauvais
  else{
    // On retourne qu'il a entrer un mauvais pwd
    printf("Wrong Password\n");
    return 0;
  } 
  return 0;
}
