#!/bin/bash

### A FAIRE EN ETANT ROOT DANS LE /home

addgroup groupe_a
addgroup groupe_b
addgroup groupe_c

### RENTRER LES INFORMATION A LA MAIN COMME LE MDP 
adduser admin --ingroup root
adduser lambda_a --ingroup groupe_a
adduser lambda_b --ingroup groupe_b

usermod -a -G groupe_a,groupe_b,group_c admin
usermod -a -G groupe_b, groupe_c lambda_b
usermod -a -G groupe_a, groupe_c lambda_a


mkdir dir_a
mkdir dir_b
mkdir dir_c

chown admin dir_a
chown admin dir_b
chown admin dir_c

chgrp groupe_c dir_c
chgrp groupe_a dir_a
chgrp groupe_b dir_b

chmod 3770 dir_a
chmod 3770 dir_b
chmod 755 dir_c

chmod g+s dir_a
chmod g+s dir_b

cd /home/lambda_a
echo "umask 0002" >> .bashrc

cd /home/lambda_b
echo "umask 0002" >> .bashrc

cd /home/admin
echo "umask 0002" >> .bashrc




