#!/bin/bash

cd ~
cd ..

# Nous sommes dans /home # il faut qu'il y est déja dans dir_a et dir_c un ficher toto.txt

cd dir_b
# On peut modifier et lire un fichier dans dir_b

touch toto.txt

cat toto.txt && echo "TEST REUSSI 1"

echo lol >> toto.txt && echo "TEST REUSSI 2"

# On peut creer et lire, modifier un sous-répertoire dans dir_b

mkdir test && echo "TEST 3 REUSSI"

cd test && touch foo.txt && echo "TEST REUSSI 4"

# Peut enlever un fichier qui lui appartient

rm -f foo.txt && echo "TEST REUSSI 5"

# Ne peut pas lire, modifier, effacer dans dir_a

cd /home/dir_a || echo "TEST REUSSI 6"

cat /home/dir_a/toto.txt || echo "TEST REUSSI 7"

echo lol >> /home/dir_a/toto.txt || echo "TEST 8 REUSSI"

# Il peut lire dans dir_c

cd /home/dir_c
cat toto.txt && echo "TEST 10 REUSSI"

# Il ne peut pas effacer, renommer, modifier dans dir_c

echo lol >> toto.txt || echo "TEST 11 REUSSI"

mv toto.txt titi.txt || echo "TEST 12 REUSSI"

touch tata.txt || echo "TEST 13 REUSSI"
