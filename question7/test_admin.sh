#!/bin/bash

cd ~
cd ..

# Nous sommes dans /home

cd dir_a
# On peut modifier et lire un fichier dans dir_a

cat toto.txt && echo "TEST REUSSI 1"

echo lol >> toto.txt && echo "TEST REUSSI 2"

# On peut creer et lire, modifier un sous-répertoire dans dir_a

mkdir test && echo "TEST 3 REUSSI"

cd test && touch foo.txt && echo "TEST REUSSI 4"

# Peut enlever un fichier 

rm -f foo.txt && echo "TEST REUSSI 5"

#Il peut lire, modifier, effacer dans dir_b

cd /home/dir_b && echo "TEST REUSSI 6"

cat /home/dir_b/toto.txt && echo "TEST REUSSI 7"

echo lol >> /home/dir_b/toto.txt && echo "TEST 8 REUSSI"

# Il peut lire dans dir_c

cd /home/dir_c
cat toto.txt && echo "TEST 10 REUSSI"

# Il ne peut pas effacer, renommer, modifier dans dir_c

echo lol >> toto.txt && echo "TEST 11 REUSSI"

mv toto.txt titi.txt && echo "TEST 12 REUSSI"

touch tata.txt && echo "TEST 13 REUSSI"
