mkdir Desktop
ls -al Desktop
#drwxr-xr-x 2 root root 4096 Jan 20 15.35 .
#drwx------ 7 root root 4096 Jan 20 15.35 ..
addgroup ubuntu
adduser --disabled-password --ingroup ubuntu --gecos "" toto
cd Desktop
touch titi.txt
ls -al
#-rw-r--r-- 1 root root    0 Jan 20 15.40 titi.txt
chmod u-w titi.txt
chmod g+w titi.txt
#-r--rw-r-- 1 root root    0 Jan 20 15.40 titi.txt
chown toto:ubuntu titi.txt
ls -al
#-r--rw-r-- 1 toto ubuntu    0 Jan 20 15.40 titi.txt
su toto
groups
#ubuntu
echo "write" > titi.txt
#bash: titi.txt: Permission denied
chmod u+w titi.txt
ls -al
#-rw-rw-r-- 1 toto ubuntu    0 Jan 20 15.40 titi.txt
echo "write" > titi.txt
cat titi.txt
#write
