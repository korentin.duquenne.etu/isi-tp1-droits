#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>


int main(int argc, char *argv[]){
  FILE *f;
  uid_t i = getuid();
  uid_t j = geteuid();
  uid_t k = getgid();
  uid_t l = getegid();
  int carac = 0;
  printf(" ruid : %d, euid : %d, rgid : %d, egid : %d",i,j,k,l);
  if (argc < 2){
    printf("Missing argument\n");
    exit(EXIT_FAILURE);
  }
  
  f = fopen(argv[1],"r");
  if(f==NULL){
    perror("Cannot Open file");
    exit(EXIT_FAILURE);
  }
  printf("File opens correctly\n");
  do{
    carac = fgetc(f);
    printf("%c",carac);
  }while(carac != EOF);
  fclose(f);
  exit(EXIT_SUCCESS);
}
